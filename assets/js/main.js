$(document).ready(function() {
	$('#owl-slider-aptn').owlCarousel({
		singleItem : true,
		navigation : false,
		pagination : true,
		autoPlay : true
	});

	$('#nanoGallery3').nanoGallery({
		thumbnailWidth : 158,
		thumbnailHeight : 89,
		locationHash : false,
		thumbnailAlignment : 'left',
		thumbnailLabel : {
			hideIcons : true,
			display : true,
			position : 'overImageonBottom'
		},
		itemsSelectable : true,
		keepSelection : true,
		paginationMaxLinesPerPage : 2,
		thumbnailHoverEffect : [ {
			'name' : 'slideUp',
			'duration' : 300
		}, {
			'name' : 'borderLighter'
		} ]
	});

	/*setTimeout(function() {
		$('.nanoGalleryPagination .paginationNext').text(">>");
	}, 200);*/
	
	// 	Gallery options
	
	var $grid = $('.grid').isotope({
		// options
		itemSelector : '.grid-item',
		layoutMode : 'fitRows'
	});
	
	$('#shows .filter-button-group ul li').click(function() {
		$('#shows .filter-button-group ul').find('.gallery_button_selected').removeClass('gallery_button_selected');
		$(this).find('button').addClass('gallery_button_selected');
	});

	$('.filter-button-group').on('click', 'button', function() {
		var filterValue = $(this).attr('data-filter');
		$grid.isotope({
			filter : filterValue
		});
	});

});